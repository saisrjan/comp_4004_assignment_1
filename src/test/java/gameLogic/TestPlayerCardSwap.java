package gameLogic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.Card;
import comp_4004_assignment_1.Game;
import comp_4004_assignment_1.Player;

class TestPlayerCardSwap {
	Game game;
	
	@BeforeEach
	public void beforeEach() {
		System.out.println("@TestInitialization: beforeEach");
		
		game = new Game();
		game.deal();
	}
	
	@AfterEach
	public void afterEach() {
		System.out.println("@TestInitialization: afterEach()");
		
		game = null;
	}

	@Test
	void test() {
		System.out.println("@TestInitialization: testGameInitialization()");
		
		Player aip = game.getAIP();
		assertEquals(5, aip.getHand().length);
		
		Card[] table = game.getTable();
		assertEquals(5, table.length);
		
		Card aipCard = aip.getHand()[0];
		Card tableCard = table[0];
		
		aip.swap(aipCard, tableCard);
		assertEquals(aip.getHand()[0].getRank(), tableCard.getRank());
		assertEquals(aip.getHand()[0].getSuit(), tableCard.getSuit());
		assertEquals(table[0].getRank(), aipCard.getRank());
		assertEquals(table[0].getSuit(), aipCard.getSuit());
		
		assertEquals(5, aip.getHand().length);
		assertEquals(5, table.length);
		
		aipCard = aip.getHand()[3];
		tableCard = table[2];
		
		aip.swap(aipCard, tableCard);
		assertEquals(aip.getHand()[3].getRank(), tableCard.getRank());
		assertEquals(aip.getHand()[3].getSuit(), tableCard.getSuit());
		assertEquals(table[2].getRank(), aipCard.getRank());
		assertEquals(table[2].getSuit(), aipCard.getSuit());
		
		assertEquals(5, aip.getHand().length);
		assertEquals(5, table.length);
		
		aipCard = aip.getHand()[4];
		tableCard = table[4];
		
		aip.swap(aipCard, tableCard);
		assertEquals(aip.getHand()[4].getRank(), tableCard.getRank());
		assertEquals(aip.getHand()[4].getSuit(), tableCard.getSuit());
		assertEquals(table[4].getRank(), aipCard.getRank());
		assertEquals(table[4].getSuit(), aipCard.getSuit());
		
		assertEquals(5, aip.getHand().length);
		assertEquals(5, table.length);
		
		aipCard = new Card("Random");
		tableCard = table[4];
		
		aip.swap(aipCard, tableCard);
		assertEquals(table[4].getRank(), tableCard.getRank());
		assertEquals(table[4].getSuit(), tableCard.getSuit());
		
		assertEquals(5, aip.getHand().length);
		assertEquals(5, table.length);
	}

}
