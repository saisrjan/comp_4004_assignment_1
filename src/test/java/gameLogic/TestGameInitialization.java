package gameLogic;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Deque;

import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.Card;
import comp_4004_assignment_1.Game;
import comp_4004_assignment_1.Player;

class TestGameInitialization {
	Game game;
	
	@Test
	void testGameInitlization() {
		System.out.println("@TestInitialization(): testGameInitialization");
		
		game = new Game();
		
		Deque<Card> deque = game.getPokerDeque();
		assertEquals(52, deque.size());
		
		game.deal();
		
		Player aip = game.getAIP();
		assertEquals(5, aip.getHand().length);
		
		Player opponent = game.getOpponent();
		assertEquals(5, opponent.getHand().length);
		
		Card[] table = game.getTable();
		assertEquals(5, table.length);
		
		assertEquals(37, deque.size());
	}
}
