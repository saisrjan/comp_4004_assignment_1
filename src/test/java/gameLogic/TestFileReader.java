package gameLogic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import comp_4004_assignment_1.Card;

public class TestFileReader {
	private BufferedReader bufferedReader;
	
	public TestFileReader(String fileName) {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<List<Card[]>> getHands() {
		List<List<Card[]>> handsTest = new ArrayList<List<Card[]>>();
		
		try {
			String line;
			
			while ((line = bufferedReader.readLine()) != null) {
				List<Card[]> hands = new ArrayList<Card[]>();
				
				line = line.trim();
				String[] lineHands = line.split(" ");
				for (String handStr : lineHands) {
					Card[] hand = new Card[5]; 
					
					String[] cardsStr = handStr.split(",");
					for (int i = 0; i < 5; i++) {
						String cardStr = cardsStr[i];
						Card card = new Card(cardStr);
						
						hand[i] = card;
					}
					
					hands.add(hand);
				}
				handsTest.add(hands);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return handsTest;
	}
}
