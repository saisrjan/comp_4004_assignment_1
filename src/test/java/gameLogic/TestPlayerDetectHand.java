package gameLogic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.Card;
import comp_4004_assignment_1.Game;
import comp_4004_assignment_1.Player;

public class TestPlayerDetectHand {
	private static TestFileReader testFileReader;
	private static List<List<Card[]>> testHands;

	@BeforeAll
	public static void beforeAll() {
		System.out.println("@TestPlayerDetectHand: beforeAll()");
		testFileReader = new TestFileReader("test.txt");
		testHands = testFileReader.getHands();
	}
	
	@BeforeEach
	public void beforeEach() {
		System.out.println("@TestPlayerDetectHand: beforeEach()");
		
	}
	
	@AfterEach
	public void afterEach() {
		System.out.println("@TestPlayerDetectHand: afterEach()");
	}
	
	@AfterAll
	public static void afterAll() {
		System.out.println("@TestPlayerDetectHand: afterAll()");
		
		testFileReader = null;
		testHands = null;
	}

	@Test
	void testDetectRoyalFlush() {
		System.out.println("@TestPlayerDetectHand: testDetectRoyalFlush()");
		Game game = new Game();
		List<Card[]> hands = testHands.get(0);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			
			boolean hasRoyalFlush = player.hasRoyalFlush();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasRoyalFlush);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasRoyalFlush);
			}
			else {
				assertEquals(false, hasRoyalFlush);
			}
		}
	}
	
	@Test
	void testDetectStraightFlush() {
		System.out.println("@TestPlayerDetectHand: testDetectStraightFlush()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(1);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			
			boolean hasStraightFlush = player.hasStraightFlush();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasStraightFlush);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasStraightFlush);
			}
			else {
				assertEquals(false, hasStraightFlush);
			}
		}
	}
	
	@Test
	void testDetectFourOfAKind() {
		System.out.println("@TestPlayerDetectHand: testDetectFourOfAKind()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(2);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			
			boolean hasFourOfAKind = player.hasFourOfAKind();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasFourOfAKind);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasFourOfAKind);
			}
			else {
				assertEquals(false, hasFourOfAKind);
			}
		}
	}
	
	@Test
	void testDetectFlush() {
		System.out.println("@TestPlayerDetectHand: testDetectFlush()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(3);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			
			boolean hasFlush = player.hasFlush();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasFlush);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasFlush);
			}
			else {
				assertEquals(false, hasFlush);
			}
		}
	}
	
	@Test
	void testDetectStraight() {
		System.out.println("@TestPlayerDetectHand: testDetectStraight()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(4);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasStraight = player.hasStraight();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasStraight);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasStraight);
			}
			else {
				assertEquals(false, hasStraight);
			}
		}
	}
	
	@Test
	void testDetectThreeOfAKind() {
		System.out.println("@TestPlayerDetectHand: testDetectThreeOfAKind()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(5);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasThreeOfAKind = player.hasThreeOfAKind();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasThreeOfAKind);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasThreeOfAKind);
			}
			else {
				assertEquals(false, hasThreeOfAKind);
			}
		}
	}
	

	@Test
	void testDetectTwoPair() {
		System.out.println("@TestPlayerDetectHand: testDetectTwoPair()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(6);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasTwoPair = player.hasTwoPair();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasTwoPair);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasTwoPair);
			}
			else {
				assertEquals(false, hasTwoPair);
			}
		}
	}
	
	@Test
	void testDetectPair() {
		System.out.println("@TestPlayerDetectHand: testDetectPair()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(7);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasPair = player.hasPair();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasPair);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasPair);
			}
			else {
				assertEquals(false, hasPair);
			}
		}
	}
	
	@Test
	void testOneCardAwayFromRoyalFlush() {
		System.out.println("@TestPlayerDetectHand: testOneCardAwayFromRoyalFlush()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(8);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean isOneCardAwayFromRoyalFlush = player.isOneCardAwayFromRoyalFlush();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(isOneCardAwayFromRoyalFlush);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, isOneCardAwayFromRoyalFlush);
			}
			else {
				assertEquals(false, isOneCardAwayFromRoyalFlush);
			}
		}
	}
	
	@Test
	void testOneCardAwayFromStraightFlush() {
		System.out.println("@TestPlayerDetectHand: testOneCardAwayFromStraightFlush()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(9);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean isOneCardAwayFromStraightFlush = player.isOneCardAwayFromStraightFlush();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(isOneCardAwayFromStraightFlush);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, isOneCardAwayFromStraightFlush);
			}
			else {
				assertEquals(false, isOneCardAwayFromStraightFlush);
			}
		}
	}
	
	@Test
	void testOneCardAwayFromFlush() {
		System.out.println("@TestPlayerDetectHand: testOneCardAwayFromSFlush()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(10);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean isOneCardAwayFromFlush = player.isOneCardAwayFromFlush();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(isOneCardAwayFromFlush);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, isOneCardAwayFromFlush);
			}
			else {
				assertEquals(false, isOneCardAwayFromFlush);
			}
		}
	}
	
	@Test
	void hasExactlyThreeSameSuit() {
		System.out.println("@TestPlayerDetectHand: hasExactlyThreeSameSuit()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(11);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasExactlyThreeSameSuit = player.hasExactlyThreeSameSuit();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasExactlyThreeSameSuit);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasExactlyThreeSameSuit);
			}
			else {
				assertEquals(false, hasExactlyThreeSameSuit);
			}
		}
	}
	
	@Test
	void hasThreeOfSameRank() {
		System.out.println("@TestPlayerDetectHand: hasThreeOfSameRank()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(12);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasThreeOfSameRank = player.hasThreeOfSameRank();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasThreeOfSameRank);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasThreeOfSameRank);
			}
			else {
				assertEquals(false, hasThreeOfSameRank);
			}
		}
	}
	
	@Test
	void hasExactlyThreeCardsInSequence() {
		System.out.println("@TestPlayerDetectHand: hasExactlyThreeCardsInSequence()");
		
		Game game = new Game();
		List<Card[]> hands = testHands.get(13);
		for (int i = 0; i < hands.size(); i++) {
			Card[] hand = hands.get(i);
			Player player = new Player(game, hand);
			boolean hasExactlyThreeCardsInSequence = player.hasExactlyThreeCardsInSequence();
			for (Card c : hand)
				System.out.print(c + ", ");
			System.out.println(hasExactlyThreeCardsInSequence);
			
			if (i < (hands.size() - 2)) {
				assertEquals(true, hasExactlyThreeCardsInSequence);
			}
			else {
				assertEquals(false, hasExactlyThreeCardsInSequence);
			}
		}
	}
}
