package gameObjects;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.Card;
import comp_4004_assignment_1.Game;
import comp_4004_assignment_1.Player;

class TestPlayer {
	Card card1, card2, card3, card4, card5;
	Card[] hand;
	
	Player player;
	Game game;
	
	@BeforeEach
	public void beforeEach() {
		System.out.println("@BeforeEach(): TestPlayer");
		card1 = new Card("SA");
		card2 = new Card("CK");
		card3 = new Card("HQ");
		card4 = new Card("DJ");
		card5 = new Card("S4");
		
		hand = new Card[5];
		hand[0] = card1;
		hand[1] = card2;
		hand[2] = card3;
		hand[3] = card4;
		hand[4] = card5;
		
		game = new Game();
	
		System.out.println("@TestPlayer(): Cards and hand intialized");
	}
	
	@AfterEach
	public void afterEach() {
		System.out.println("@TestPlayer(): TestPokerDeque");
		card1 = null;
		card2 = null;
		card3 = null;
		card4 = null;
		card5 = null;
		
		hand = null;
		
		System.out.println("@TestPlayer(): Cards and hand deintialized");
	}
	
	@Test
	void testHand() {
		System.out.println("@TestPlayer(): testHand");
		player = new Player(game, hand);
		Card[] playerHand = player.getHand();
		
		assertEquals(5, playerHand.length);
		player = null;
	}
}
