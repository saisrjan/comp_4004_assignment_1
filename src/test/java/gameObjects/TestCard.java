package gameObjects;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.Card;

class TestCard {
	Card card1, card2, card3, card4, card5, card6, card7, card8, card9, 
	card10, card11, card12;
	
	@BeforeEach
	public void beforeAll() {
		System.out.println("@BeforeEach(): TestCard");
		card1 = new Card("SA");
		card2 = new Card("CK");
		card3 = new Card("HQ");
		card4 = new Card("DJ");
		card5 = new Card("S4");
		card6 = new Card("C5");
		card7 = new Card("H6");
		card8 = new Card("D7");
		card9 = new Card("G7");
		card10 = new Card("S*");
		card11 = new Card("Y&");
		card12 = new Card("RandomValue");
		System.out.println("@BeforeEach(): Cards initialized");
	}
	
	@AfterEach
	public void tearDown() {
		System.out.println("@AfterEach(): TestCard");
		card1 = null;
		card2 = null;
		card3 = null;
		card4 = null;
		card5 = null;
		card6 = null;
		card7 = null;
		card8 = null;
		card9 = null;
		card10 = null;
		card11 = null;
		card12 = null;
		System.out.println("@AfterEach(): Cards deinitialized");
	}
	
	@Test
	void testCard() {
		System.out.println("@Test(): TestCard");
		assertEquals(card1.getSuit(), Card.SPADES);
		assertEquals(card1.getRank(), Card.ACE);
		assertEquals("SA", card1.toString());
		
		assertEquals(card2.getSuit(), Card.CLUBS);
		assertEquals(card2.getRank(), Card.KING);
		assertEquals("CK", card2.toString());
		
		assertEquals(card3.getSuit(), Card.HEARTS);
		assertEquals(card3.getRank(), Card.QUEEN);
		assertEquals("HQ", card3.toString());
		
		assertEquals(card4.getSuit(), Card.DIAMONDS);
		assertEquals(card4.getRank(), Card.JACK);
		assertEquals("DJ", card4.toString());

		assertEquals(card5.getSuit(), Card.SPADES);
		assertEquals(card5.getRank(), 4);
		assertEquals("S4", card5.toString());
		
		assertEquals(card6.getSuit(), Card.CLUBS);
		assertEquals(card6.getRank(), 5);
		assertEquals("C5", card6.toString());
		
		assertEquals(card7.getSuit(), Card.HEARTS);
		assertEquals(card7.getRank(), 6);
		assertEquals("H6", card7.toString());
		
		assertEquals(card8.getSuit(), Card.DIAMONDS);
		assertEquals(card8.getRank(), 7);
		assertEquals("D7", card8.toString());
		
		assertEquals(card9.getSuit(), 0);
		assertEquals(card9.getRank(), 7);
		assertEquals("G7", card9.toString());
		
		assertEquals(card10.getSuit(), Card.SPADES);
		assertEquals(card10.getRank(), 0);
		assertEquals("S*", card10.toString());
		
		assertEquals(card11.getSuit(), 0);
		assertEquals(card11.getRank(), 0);
		assertEquals("Y&", card11.toString());
		
		assertEquals(card12.getSuit(), 0);
		assertEquals(card12.getRank(), 0);
		assertEquals("RandomValue", card12.toString());
	}
}
