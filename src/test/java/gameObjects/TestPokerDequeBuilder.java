package gameObjects;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.PokerDeque;
import comp_4004_assignment_1.PokerDequeBuilder;

class TestPokerDequeBuilder {
	String full_deque_file = "full_deque.txt";
	String half_deque_file = "half_deque.txt";
	String empty_deque_file = "empty_deque.txt";
	
	static PokerDequeBuilder dequeBuilder;
	static PokerDeque deque;
	
	@BeforeAll
	public static void beforeAll() {
		System.out.println("@BeforeAll: TestPokerDequeBuilder");
		dequeBuilder = new PokerDequeBuilder();
	}
	
	@AfterAll
	public static void afterAll() {
		System.out.println("@AfterAll: TestPokerDequeBuilder");
		dequeBuilder = null;
		
		deque.clear();
		deque = null;
	}

	@Test
	void testInitializeFullDeque() {
		deque = dequeBuilder.buildDeque(full_deque_file);
		assertEquals(52, deque.size());
	}
	
	@Test
	void testInitializeHalfDeque() {
		deque = dequeBuilder.buildDeque(half_deque_file);
		assertEquals(25, deque.size());
	}
	
	@Test
	void testInitializeEmptyDeque() {
		deque = dequeBuilder.buildDeque(empty_deque_file);
		assertEquals(0, deque.size());
	}
}
