package gameObjects;

import static org.junit.Assert.assertEquals;

import java.util.Deque;
import java.util.LinkedList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import comp_4004_assignment_1.Card;
import comp_4004_assignment_1.PokerDeque;

class TestPokerDeque {
	Card card1, card2, card3, card4, card5, card6, card7, card8,
	card9;
	
	PokerDeque deque;
	Deque<Card> orderedDeque;
	
	@BeforeEach
	public void beforeEach() {
		System.out.println("@BeforeEach(): TestPokerDeque");
		card1 = new Card("SA");
		card2 = new Card("CK");
		card3 = new Card("HQ");
		card4 = new Card("DJ");
		card5 = new Card("S4");
		card6 = new Card("C5");
		card7 = new Card("H6");
		card8 = new Card("D7");
		card9 = new Card("G7");
		
		deque = new PokerDeque();
		orderedDeque = new LinkedList<Card>();
		
		deque.add(card1);
		deque.add(card2);
		deque.add(card3);
		deque.add(card4);
		deque.add(card5);
		deque.add(card6);
		deque.add(card7);
		deque.add(card8);
		deque.add(card9);
		
		orderedDeque.add(card1);
		orderedDeque.add(card2);
		orderedDeque.add(card3);
		orderedDeque.add(card4);
		orderedDeque.add(card5);
		orderedDeque.add(card6);
		orderedDeque.add(card7);
		orderedDeque.add(card8);
		orderedDeque.add(card9);
		
		System.out.println("@BeforeEach(): Cards and deques intialized");
	}
	
	@AfterEach
	public void afterEach() {
		System.out.println("@AfterEach(): TestPokerDeque");
		card1 = null;
		card2 = null;
		card3 = null;
		card4 = null;
		card5 = null;
		card6 = null;
		card7 = null;
		card8 = null;
		card9 = null;
		
		deque.clear();
		deque = null;
		
		orderedDeque.clear();
		orderedDeque = null;
		
		System.out.println("@AfterEach(): Cards and deques deintialized");
	}
	
	@Test
	void testPokerDequeShuffle() {
		deque.shuffle();
		
		boolean sameOrder = true;
		byte differentCards = 0;
		
		for (int i = 0; i < orderedDeque.size(); i++) {
			Card card1 = deque.pop();
			Card card2 = orderedDeque.pop();
			
			if ((card1.getSuit() != card2.getSuit()) || (card1.getRank() != card2.getRank()))
				differentCards++;
		}
		
		if (differentCards > 0)
			sameOrder = false;
		
		assertEquals(false, sameOrder);
	}
}
