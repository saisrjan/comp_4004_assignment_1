package comp_4004_assignment_1;

public class Interface {
	public static void printGame(Game game) {
		Player opponent = game.getOpponent();
		Player aip = game.getAIP();
		
		Card[] opponentHand = opponent.getHand();
		System.out.print("Opponent: ");
		for (Card c : opponentHand) {
			System.out.print(c + ", ");
		}
		System.out.println("");
		
		Card[] table = game.getTable();
		System.out.print("Table: ");
		for (Card c : table) {
			System.out.print(c + ", ");
		}
		System.out.println("");
		
		Card[] aipHand = aip.getHand();
		System.out.print("AIP: ");
		for (Card c : aipHand) {
			System.out.print(c + ", ");
		}
		System.out.println("");
	}
	public static void main(String[] args) {
		Game game = new Game();
		game.deal();
		
		printGame(game);
	}
}
