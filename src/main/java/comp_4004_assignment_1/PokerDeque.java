package comp_4004_assignment_1;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;

public class PokerDeque extends ArrayDeque<Card> {
	private static final long serialVersionUID = 4898780661240103217L;

	public void shuffle() {
		Deque<Card> shuffleDeque = new LinkedList<Card>();
		
		Random rand = new Random();
		
		int copy;
		while (!this.isEmpty()) {
			copy = rand.nextInt(10);
			
			Card card = this.pop();
			if (copy < 5)
				shuffleDeque.add(card);
			else
				this.addLast(card);
		}
		
		while (!shuffleDeque.isEmpty()) {
			this.push(shuffleDeque.pop());
		}
	}
}
