package comp_4004_assignment_1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PokerDequeBuilder {
	public PokerDequeBuilder() {
		
	}
	
	public PokerDeque buildDeque(String dequeFile) {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(dequeFile).getFile());
		
		PokerDeque deque = new PokerDeque();
		
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				Card card = new Card(line.trim());
				deque.push(card);
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return deque;
	}
}
