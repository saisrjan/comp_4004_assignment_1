package comp_4004_assignment_1;

public class Card {
	public static byte CLUBS = 1;
	public static byte DIAMONDS = 2;
	public static byte HEARTS = 3;
	public static byte SPADES = 4;
	
	public static byte JACK = 11;
	public static byte QUEEN = 12;
	public static byte KING = 13;
	public static byte ACE = 14;
	
	private byte suit;
	private byte rank;
	private String cardStr;
	
	public Card(String cardStr) {
		suit = 0;
		rank = 0;
		
		if (cardStr.length() == 2 || cardStr.length() == 3) {
			char suitChar = cardStr.charAt(0);
			
			switch (suitChar) {
				case 'S':
					suit = SPADES;
					break;
				case 'H':
					suit = HEARTS;
					break;
				case 'D':
					suit = DIAMONDS;
					break;
				case 'C':
					suit = CLUBS;
					break;
			}
			
			String rankStr = null;
			if (cardStr.length() == 2)
				rankStr = cardStr.substring(1, 2);
			else
				rankStr = cardStr.substring(1, 3);
			
			switch (rankStr) {
				case "J":
					rank = JACK;
					break;
				case "Q":
					rank = QUEEN;
					break;
				case "K":
					rank = KING;
					break;
				case "A":
					rank = ACE;
					break;
				default:
					try {
						rank = Byte.parseByte(rankStr);
					} catch (NumberFormatException e) {
						rank = 0;
					}
					break;
			}
		}
		
		this.cardStr = cardStr;
	}
	
	public byte getSuit() {
		return suit;
	}
	
	public byte getRank() {
		return rank;
	}
	
	@Override
	public String toString() {
		return cardStr;
	}
}
