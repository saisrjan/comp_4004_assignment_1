package comp_4004_assignment_1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Player {
	private Card[] hand;
	private Game game;
	
	public Player(Game game, Card[] hand) {
		this.game = game;
		this.hand = hand;
	}
	
	public void swap(Card cardFromHand, Card cardAtTable) {
		Card[] table = game.getTable();
		
		int indexHand = -1;
		int indexTable = -1;
		
		for (int i = 0; i < hand.length; i++) {
			Card c = hand[i];
			if ((c.getRank() == cardFromHand.getRank()) && 
					(c.getSuit() == cardFromHand.getSuit())) {
				indexHand = i;
				break;
			}
		}
		
		for (int i = 0; i < table.length; i++) {
			Card c = table[i];
			if ((c.getRank() == cardAtTable.getRank()) && 
					(c.getSuit() == cardAtTable.getSuit())) {
				indexTable = i;
				break;
			}
		}
		
		if ((indexHand != -1) && (indexTable != -1)) {
			Card temp = hand[indexHand];
			hand[indexHand] = table[indexTable];
			table[indexTable] = temp;
		}
	}
	
	public boolean hasRoyalFlush() {
		boolean ace = false;
		boolean king = false;
		boolean queen = false;
		boolean jack = false;
		boolean ten = false;
		
		byte suit = 0;
		for (Card c : hand) {
			if (suit == 0)
				suit = c.getSuit();
			
			if (suit != c.getSuit())
				return false;
			
			if (c.getRank() == Card.ACE)
				ace = true;
			else if (c.getRank() == Card.KING)
				king = true;
			else if (c.getRank() == Card.QUEEN)
				queen = true;
			else if (c.getRank() == Card.JACK)
				jack = true;
			else if (c.getRank() == 10)
				ten = true;
		}
		
		if (ace && king && queen && jack && ten)
			return true;
					
		return false;
	}
	
	public boolean hasStraightFlush() {
		byte suit = 0;
		byte[] ranks = new byte[5];
		
		int two = -1;
		int ace = -1;
		for (int i = 0; i < ranks.length; i++) {
			Card c = hand[i];
			if (suit == 0)
				suit = c.getSuit();
			
			if (suit != c.getSuit())
				return false;
			
			if (c.getRank() == 2)
				two = i;
			
			if (c.getRank() == Card.ACE)
				ace = i;
			
			ranks[i] = c.getRank();
		}
		
		if (two != -1 && ace != -1) {
			ranks[ace] = 1;
		}
		
		Arrays.sort(ranks);
		
		byte prevRank = 0;
		for (byte r : ranks) {
			if (prevRank == 0) {
				prevRank = r;
			}
			else {
				if ((r - prevRank) > 1)
					return false;
				
				prevRank = r;
			}
		}
		
		return true;
	}
	
	public boolean hasFourOfAKind() {
		HashMap<Byte, Integer> rankCounter = new HashMap<Byte, Integer>();
		
		for (Card c : hand) {
			if (!(rankCounter.containsKey(c.getRank()))) {
				rankCounter.put(c.getRank(), 1);
				continue;
			}
			
			int counter = rankCounter.get(c.getRank());
			rankCounter.put(c.getRank(),  ++counter);
		}
		
		if (rankCounter.values().contains(4))
			return true;
		
		return false;
	}
	
	public boolean hasFlush() {
		byte prevSuit = 0;
		
		for (Card c : hand) {
			if (prevSuit == 0)
				prevSuit = c.getSuit();
			
			if (prevSuit != c.getSuit())
				return false;
		}
		
		return true;
	}
	
	public boolean hasStraight() {
		HashMap<Byte, Integer> suitCounter = new HashMap<Byte, Integer>();
		
		for (Card c : hand) {
			if (!(suitCounter.containsKey(c.getSuit()))) {
				suitCounter.put(c.getSuit(), 1);
				continue;
			}
			
			int counter = suitCounter.get(c.getSuit());
			suitCounter.put(c.getSuit(),  ++counter);
		}
		
		if (suitCounter.values().contains(5))
			return false;
		
		byte[] ranks = new byte[5];
		
		int two = -1;
		int ace = -1;
		for (int i = 0; i < ranks.length; i++) {
			Card c = hand[i];
			
			if (c.getRank() == 2)
				two = i;
			
			if (c.getRank() == Card.ACE)
				ace = i;
			
			ranks[i] = c.getRank();
		}
		
		if (two != -1 && ace != -1) {
			ranks[ace] = 1;
		}
		
		Arrays.sort(ranks);
		
		byte prevRank = 0;
		for (byte r : ranks) {
			if (prevRank == 0) {
				prevRank = r;
			}
			else {
				if ((r - prevRank) > 1)
					return false;
				
				prevRank = r;
			}
		}
		
		return true;
	}
	
	public boolean hasThreeOfAKind() {
		HashMap<Byte, Integer> rankCounter = new HashMap<Byte, Integer>();
		
		for (Card c : hand) {
			if (!(rankCounter.containsKey(c.getRank()))) {
				rankCounter.put(c.getRank(), 1);
				continue;
			}
			
			int counter = rankCounter.get(c.getRank());
			rankCounter.put(c.getRank(),  ++counter);
		}
		
		if (rankCounter.values().contains(3))
			return true;
		
		return false;
	}
	
	public boolean hasTwoPair() {
		HashMap<Byte, Integer> rankCounter = new HashMap<Byte, Integer>();
		
		for (Card c : hand) {
			if (!(rankCounter.containsKey(c.getRank()))) {
				rankCounter.put(c.getRank(), 1);
				continue;
			}
			
			int counter = rankCounter.get(c.getRank());
			rankCounter.put(c.getRank(),  ++counter);
		}
		
		int numOfPairs = 0;
		for (byte r : rankCounter.keySet()) {
			if (rankCounter.get(r) == 2)
				numOfPairs++;
		}
		
		return numOfPairs == 2;
	}
	
	public boolean hasPair() {
		HashMap<Byte, Integer> rankCounter = new HashMap<Byte, Integer>();
		
		for (Card c : hand) {
			if (!(rankCounter.containsKey(c.getRank()))) {
				rankCounter.put(c.getRank(), 1);
				continue;
			}
			
			int counter = rankCounter.get(c.getRank());
			rankCounter.put(c.getRank(),  ++counter);
		}
		
		int numOfPairs = 0;
		for (byte r : rankCounter.keySet()) {
			if (rankCounter.get(r) == 2)
				numOfPairs++;
		}
		
		return numOfPairs == 1;
	}
	
	public boolean isOneCardAwayFromRoyalFlush() {
		Set<Byte> suitSet = new HashSet<Byte>();
		Set<Byte> rankSet = new HashSet<Byte>();
		
		for (Card c : hand) {
			suitSet.add(c.getSuit());
			
			if (c.getRank() == Card.ACE)
				rankSet.add(c.getRank());
			else if (c.getRank() == Card.KING)
				rankSet.add(c.getRank());
			else if (c.getRank() == Card.QUEEN)
				rankSet.add(c.getRank());
			else if (c.getRank() == Card.JACK)
				rankSet.add(c.getRank());
			else if (c.getRank() == 10)
				rankSet.add(c.getRank());
		}
				
		if ((suitSet.size() <= 2 && rankSet.size() == 4) || 
				(suitSet.size() == 2 && rankSet.size() == 5))
			return true;
			
		return false;
	}
	
	public boolean isOneCardAwayFromStraightFlush () {
		return false;
	}
	
	public boolean isOneCardAwayFromFlush() {
		HashMap<Byte, Byte> map = new HashMap<Byte, Byte>();
		for (Card c : hand) {
			if (!map.containsKey(c.getSuit())) {
				map.put(c.getSuit(), (byte)0);
			}
			
			byte b = (byte) (map.get(c.getSuit()) + (byte)1);
			map.put(c.getSuit(), b);
		}
		
		
		if (map.keySet().size() == 2) {
			for (byte suit : map.keySet()) {
				if (map.get(suit) == 1 || map.get(suit) == 4)
					return true;
				
				
			}
		}
		
		return false;
	}

	public boolean hasExactlyThreeSameSuit() {
		HashMap<Byte, Byte> map = new HashMap<Byte, Byte>();
		for (Card c : hand) {
			if (!map.containsKey(c.getSuit())) {
				map.put(c.getSuit(), (byte)0);
			}
			
			byte b = (byte) (map.get(c.getSuit()) + (byte)1);
			map.put(c.getSuit(), b);
		}
		
		
		if (map.containsValue((byte)3))
			return true;
 		
		return false;
	}
	
	public boolean hasThreeOfSameRank() {
		HashMap<Byte, Byte> map = new HashMap<Byte, Byte>();
		for (Card c : hand) {
			if (!map.containsKey(c.getRank())) {
				map.put(c.getRank(), (byte)0);
			}
			
			byte b = (byte) (map.get(c.getRank()) + (byte)1);
			map.put(c.getRank(), b);
		}
		
		
		if (map.containsValue((byte)3))
			return true;
 		
		return false;
	}

	public boolean hasExactlyThreeCardsInSequence() {
		byte[] ranks = new byte[5];
		
		int ace = -1;
		int two = -1;
		for (int i = 0; i < 5; i++) {
			ranks[i] = hand[i].getRank();
			if (hand[i].getRank() == Card.ACE)
				ace = i;
			
			if (hand[i].getRank() == 2)
				two = i;
		}
		
		if ((ace != -1) && (two != -1))
			ranks[ace] = 1;
		
		Arrays.sort(ranks);
		
		byte inc = 0;
		byte largestInc = 0;
		byte prevRank = 0;
		for (byte b : ranks) {
			if (prevRank == 0) {
				prevRank = b;
				continue;
			}
			
			if ((b - prevRank) == 1) {
				inc++;
			}
			else {
				if (largestInc < inc)
					largestInc = inc;
				
				inc = 0;
			}
			
			prevRank = b;
		}
		
		
		if (largestInc == 2)
			return true;
		
		return false;
	}
	
	public Card[] getHand() {
		return hand;
	}
}
