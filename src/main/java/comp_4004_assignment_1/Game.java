package comp_4004_assignment_1;

import java.util.Deque;

public class Game {
	private Deque<Card> deque;
	private Player aip;
	private Player opponent;
	
	private Card[] table;
	
	public Game() {
		PokerDequeBuilder pdb = new PokerDequeBuilder();
		deque = pdb.buildDeque("full_deque.txt");
	}
	
	public void deal() {
		Card[] aipHand = new Card[5];
		Card[] opponentHand = new Card[5];
		table = new Card[5];
		
		for (int i = 0; i < 5; i++) {
			aipHand[i] = deque.pop();
			opponentHand[i] = deque.pop();
			table[i] = deque.pop();
		}
		
		aip = new Player(this, aipHand);
		opponent = new Player(this, opponentHand);
	}
	
	public Deque<Card> getPokerDeque() {
		return deque;
	}
	
	public Player getAIP() {
		return aip;
	}
	
	public Player getOpponent() {
		return opponent;
	}
	
	public Card[] getTable () {
		return table;
	}
}
